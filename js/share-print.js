/**
 * @file
 * share-print.js
 *
 * Defines the behaviors needed for doghouse social media share print integration.
 */

(function ($, Drupal) {

  /**
   * Doghouse Social Media Block Share print button.
   */
  Drupal.behaviors.doghouseSocialMediaBlockSharePrint = {
    attach: function (context, settings) {
      $('.js-dsms-print', context).once('share-print').each(function(){
        $(this).click(function(e) {
          e.preventDefault();
          window.print();
        })
      });
    }
  };

}(jQuery, Drupal));
