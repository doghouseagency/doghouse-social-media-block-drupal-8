<?php

/**
 * @file
 * Hooks provided by Doghouse Social Media Block module.
 */

/**
 * Define social media platforms available to share to.
 *
 * @param array $page_info
 *   An array of all the existing plugin definitions, passed by reference.
 *
 * @return
 *   An array of social media platforms to share to.
 *
 * @see \Drupal\doghouse_social_media_block\Plugin\Block\SocialMediaShare
 */
function hook_social_media_share_info(array $page_info) {
  $url = $page_info['current_page_url'];
  $title = $page_info['current_page_title'];
  return [
    'facebook' => [
      'title' => t('Facebook'),
      'url' => "http://www.facebook.com/share.php?u={$url}",
      'attributes' => [
        'target' => '_blank',
        'aria-label' => t('Share this page on facebook.'),
        'title' => t('Share this page on facebook.'),
      ],
    ],
  ];
}

/**
 * Alter the definitions defined in hook_social_media_share_info.
 *
 * @param array $platforms
 *   An array of social media platforms to share to.
 *
 * @see \Drupal\doghouse_social_media_block\Plugin\Block\SocialMediaShare
 */
function hook_doghouse_social_media_share_info_alter(array &$platforms) {
  unset($platforms['facebook']);
}
