<?php

namespace Drupal\doghouse_social_media_block\Plugin\Block;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Template\Attribute;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'SocialMediaShare' block.
 *
 * @Block(
 *  id = "social_media_share",
 *  admin_label = @Translation("Social Media Share"),
 * )
 */
class SocialMediaShare extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Controller\TitleResolverInterface definition.
   *
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  protected $titleResolver;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->titleResolver = $container->get('title_resolver');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->requestStack = $container->get('request_stack');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'display_links' => [],
      'links_prefix' => '',
      'links_suffix' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * Social media settings.
   *
   * Returns an array of settings to define which sharing options to display
   * on the frontend.
   * These options are passed to the social_media_links variable in the
   * doghouse_social_media_share theme, so these options can be changed/added to
   * within a hook_preprocess_doghouse_social_media_share.
   *
   * @return array
   */
  public function shareSettings() {
    $page_info = [
      'current_page_url' => $this->getCurrentPageUrl(),
      'current_page_title' => $this->getPageTitle(),
    ];

    // Get a list of social media platforms to share to.
    $settings = $this->moduleHandler->invokeAll('doghouse_social_media_share_info', [$page_info]);
    // Add ability to alter any settings.
    $this->moduleHandler->alter('doghouse_social_media_share_info', $settings);

    return $settings;
  }

  /**
   * Gets the current page title.
   *
   * @return array|string|null
   */
  public function getPageTitle() {
    $title = '';
    $request = \Drupal::request();
    if ($route = $request->attributes->get(RouteObjectInterface::ROUTE_OBJECT)) {
      $title = $this->titleResolver->getTitle($request, $route);

      if ($title instanceof MarkupInterface) {
        $title = strip_tags((string) $title);
      }
      elseif (is_array($title) && array_key_exists('#markup', $title)) {
        $tags = array_key_exists('#allowed_tags', $title) ? $title['#allowed_tags'] : NULL;
        $title = Xss::filter($title['#markup'], $tags);
      }
    }
    return $title;
  }

  /**
   * Gets the current page absolute url.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   */
  public function getCurrentPageUrl() {
    return $this->requestStack->getCurrentRequest()->getUri();
  }

  /**
   * Social media sharing optons.
   *
   * Returns an options array of social sharing platforms to enable/disable
   * on the block configuration form.
   *
   * @return array
   */
  public function shareOptions() {
    $options = $this->shareSettings();
    return array_combine(array_keys($options), array_keys($options));
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['display_links'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Display links'),
      '#description' => $this->t('Select which sharing options to display'),
      '#options' => $this->shareOptions(),
      '#default_value' => $this->configuration['display_links'],
      '#weight' => '0',
    ];

    $form['links_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix links'),
      '#description' => $this->t('Some text to prefix the links with.'),
      '#default_value' => $this->configuration['links_prefix'],
      '#weight' => '1',
    ];

    $form['links_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix links'),
      '#description' => $this->t('Some text to suffix the links with.'),
      '#default_value' => $this->configuration['links_suffix'],
      '#weight' => '2',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['display_links'] = $form_state->getValue('display_links');
    $this->configuration['links_prefix'] = $form_state->getValue('links_prefix');
    $this->configuration['links_suffix'] = $form_state->getValue('links_suffix');
  }

  /**
   * Build array of links to render on the frontend.
   *
   * @return array
   */
  public function buildLinks() {
    $selected = array_filter($this->configuration['display_links']);
    $settings = $this->shareSettings();
    $links = [];

    // Filter options by those selected on the block configuration form.
    foreach ($selected as $option) {
      if (isset($settings[$option])) {
        $links[$option] = [
          'title' => $settings[$option]['title'],
          'url' => $settings[$option]['url'],
          'attributes' => new Attribute($settings[$option]['attributes']),
        ];
      }
    }

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $links = $this->buildLinks();
    $build = [];
    $build['#theme'] = 'doghouse_social_media_share';
    $build['#social_media_links'] = $links;
    $build['#links_prefix'] = $this->configuration['links_prefix'];
    $build['#links_suffix'] = $this->configuration['links_suffix'];

    $build['#cache'] = [
      'contexts' => [
        'url.path',
      ],
    ];

    // Attach print dialog javascript if the print option is enabled.
    if (array_key_exists('print', $links)) {
      $build['#attached']['library'][] = 'doghouse_social_media_block/share_print';
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

}
