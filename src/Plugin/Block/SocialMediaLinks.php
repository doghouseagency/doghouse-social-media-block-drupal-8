<?php

namespace Drupal\doghouse_social_media_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Provides a 'Social Media Links' Block.
 *
 * @Block(
 *   id = "social_media_block",
 *   admin_label = @Translation("Social media block"),
 *   category = @Translation("Social media"),
 * )
 */
class SocialMediaLinks extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * A list of social media links retrieved from the config form.
   *
   * @var array
   */
  private $socialMedia;

  /**
   * Drupals config container.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal message interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a Social media block block plugin.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Drupals config container.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, array $configuration, $plugin_id, array $plugin_definition, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->messenger = $messenger;

    // Get the social media settings from config/system/social_media.
    $social_media_config = $this->configFactory->get('doghouse_social_media_block.settings');
    if (!is_null($social_media_config)) {
      $this->socialMedia = $social_media_config->get();
      // Explode on new lines (with preg_split() rather than explode()).
      $this->socialMedia = preg_split("/\\r\\n|\\r|\\n/", $this->socialMedia['social_media_services']);
    }
    else {
      $this->messenger->addMessage($this->t('The system could not retrieve the social media links. Please check the social media settings.'), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory'),
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $config = $this->getConfiguration();

    $social_media_links = [];

    foreach ($this->socialMedia as $field) {
      $field_name = 'social_media_link_' . $field;

      if (!empty(trim($config[$field_name]))) {
        $url = Url::fromUri(
          $config[$field_name],
          [
            'attributes' => [
              'class' => [
                'icon-' . $field,
              ],
              'target' => '_blank',
              'itemprop' => 'sameAs',
            ],
          ]
        );

        $link = Link::fromTextAndUrl(
          Markup::create('<span>' . $field . '</span>'),
          $url);

        $social_media_links[] = $link->toRenderable();
      }
    }

    $build['#theme'] = 'doghouse_social_media_block';
    $build['#social_media_links'] = $social_media_links;

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $form += $this->generateSocialMediaLinkField();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    foreach ($this->socialMedia as $field) {
      $field_name = 'social_media_link_' . $field;

      $this->configuration[$field_name] = $form_state->getValue($field_name);
    }
  }

  /**
   * Custom function for generating social media link fields.
   *
   * @return array
   *   An array of configurable social media fields.
   */
  private function generateSocialMediaLinkField() {
    $fields = [];

    $config = $this->getConfiguration();

    foreach ($this->socialMedia as $field) {
      $title = ucfirst($field);
      $field_name = 'social_media_link_' . $field;

      $fields[$field_name] = [
        '#title' => $this->t('@title', ['@title' => $title]),
        '#description' => $this->t('Link to @title page. Leave blank to omit this social media link from display.', ['@title' => $title]),
        '#type' => 'textfield',
        '#default_value' => isset($config[$field_name])
        ? $config[$field_name]
        : '',
        '#required' => FALSE,
      ];
    }

    return $fields;
  }

}
