<?php

namespace Drupal\doghouse_social_media_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Social Media settings for this site.
 */
class SocialMediaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'doghouse_social_media_block_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'doghouse_social_media_block.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('doghouse_social_media_block.settings');

    $default_social_media = "facebook\r\ntwitter\r\ninstagram\r\nyoutube";

    $form['social_media_services'] = [
      '#type' => 'textarea',
      '#title' => 'Enter social media services.',
      '#default_value' => empty($config->get('social_media_services')) ?
      $default_social_media
      : $config->get('social_media_services'),
      '#description' => $this->t("Please enter each social media site name on a new line.\r\nTo restore defaults, empty this field and save."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('doghouse_social_media_block.settings')
      ->set('social_media_services', $form_state->getValue('social_media_services'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
