# Doghouse Social Media Block
# `⌒°(❛ᴗ❛)°⌒`

##### What it does
  - Creates a block called `Social Media Links`
  - All social media sites can be added in a single config
  - URLs are added to each block instance (so different block instances can show differerent links)

##### How to install
 - Ensure you have the doghouse packages repository in your `composer.json`
```
"repositories": [
        ...
        {
            "type": "composer",
            "url": "http://packages.doghouse.agency/"
        }
```
  - In terminal, run: `$ composer require doghouse/doghouse_social_media_block`
  - Enable the module (EG `drush en doghouse_social_media_block`)
  -- The module will be located in `modules/contrib` 

##### How to use it
  - Navigate to /admin/config/system/social_media/settings to set the social media sites by site name
  -- The module automatically defaults to Facebook, Twitter, Instagram & Youtube
  - Add the block "Social Media Links" in Drupal
  - Add the URLs in the block form, or leave blank to  omit from display
# `~(=^‥^)ノ☆`
